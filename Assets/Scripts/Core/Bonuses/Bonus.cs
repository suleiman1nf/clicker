using UnityEngine;

namespace Core.Bonuses
{
    public abstract class Bonus : MonoBehaviour
    {
        public abstract void GetBonus(ClickObject clickObject);
    }
}
