using System;
using Core.States;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Bonuses
{
    public class BonusController : MonoBehaviour
    {
        [SerializeField] private Bonus[] bonuses;

        private GameController gameController;
        private bool isInit = false;

        public void Init(GameController gameState)
        {
            this.gameController = gameState;
            isInit = true;
        }
        
        public void OnMouseDown()
        {
            if (!isInit)
            {
                Debug.LogError("Not Init");
            }

            foreach (var bonus in bonuses)
            {
                gameController.OnBonusClick(bonus);   
            }
            
            Destroy(gameObject);
        }
    }
}
