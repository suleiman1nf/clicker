using System.Collections;
using Core.ClickModules;
using UnityEngine;

namespace Core.Bonuses
{
    public class FreezeBonus : Bonus
    {
        private float freezeTime = 5f;
        
        public override void GetBonus(ClickObject clickObject)
        {
            StartCoroutine(ChangeMoverState(clickObject));
        }

        private IEnumerator ChangeMoverState(ClickObject clickObject)
        {
            clickObject.GetModule<Mover>().ChangeState(false);
            yield return new WaitForSeconds(freezeTime);
            clickObject.GetModule<Mover>().ChangeState(true);
        }
    }
}
