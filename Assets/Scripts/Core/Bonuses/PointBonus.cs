using System.Collections;
using UnityEngine;

namespace Core.Bonuses
{
    public class PointBonus : Bonus
    {
        [SerializeField] private int count = 2;
        [SerializeField] private float time = 5;

        public override void GetBonus(ClickObject clickObject)
        {
            StartCoroutine(Timer(clickObject));
        }

        private IEnumerator Timer(ClickObject clickObject)
        {
            clickObject.PointCount = count;
            yield return new WaitForSecondsRealtime(time);
            clickObject.PointCount = 1;
        }
    }
}
