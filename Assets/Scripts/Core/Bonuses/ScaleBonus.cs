using DG.Tweening;
using UnityEngine;

namespace Core.Bonuses
{
    public class ScaleBonus : Bonus
    {
        private float scale = 2;
        public override void GetBonus(ClickObject clickObject)
        {
            clickObject.transform.DOScale(scale * clickObject.transform.localScale, 0.5f).SetEase(Ease.OutBack);
        }
    }
}
