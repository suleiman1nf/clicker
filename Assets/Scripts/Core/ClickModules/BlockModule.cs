﻿using UnityEngine;

namespace Core.ClickModules
{
    public abstract class BlockModule : MonoBehaviour
    {
        [SerializeField] private bool isActive = true;
        protected abstract void ExecuteModule(GameController controller);
        public void Execute(GameController controller)
        {
            if(isActive)
                ExecuteModule(controller);
        }

        public void ChangeState(bool state)
        {
            isActive = state;
        }
    }
}