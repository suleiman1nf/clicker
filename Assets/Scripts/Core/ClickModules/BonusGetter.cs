﻿namespace Core.ClickModules
{
    public class BonusGetter : BlockModule
    {
        protected override void ExecuteModule(GameController controller)
        {
            controller.CurrProgress += controller.clickObject.PointCount;
        }
    }
}