using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.ClickModules
{
    public class Mover : BlockModule
    {
        public void Move()
        {
            var minX = -3.1f;
            var maxX = 1.5f;
            var minY = -4.5f;
            var maxY = 3f;
            float time = 0.2f;
            Vector3 vec = new Vector3(Random.Range(minX, maxX), transform.localPosition.y, Random.Range(minY, maxY));
            transform.DOLocalMove(vec, time);
        }

        protected override void ExecuteModule(GameController controller)
        {
            Move();
        }
    }
}
