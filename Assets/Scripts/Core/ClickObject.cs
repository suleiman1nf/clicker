using System;
using System.Collections.Generic;
using Core.ClickModules;
using UnityEngine;

namespace Core
{
    public class ClickObject : MonoBehaviour
    {
        [SerializeField] private List<BlockModule> modules;
        public event Action<ClickObject> OnClick;

        private int pointCount = 1;

        public int PointCount
        {
            get => pointCount;
            set => pointCount = value;
        }

        public void ExecuteModules(GameController controller)
        {
            foreach (var module in modules)
            {
                module.Execute(controller);
            }
        }

        public T GetModule<T>() where T : BlockModule
        {
            foreach (var blockModule in modules)
            {
                if (blockModule.GetType() == typeof(T))
                {
                    return (T)blockModule;
                }
            }

            return null;
        }
        
        private void OnMouseDown()
        {
            OnClick?.Invoke(this);
        }
    }
}
