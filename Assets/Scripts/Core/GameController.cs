using System;
using System.Collections.Generic;
using Core.Bonuses;
using Core.UI;
using Pixelplacement;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core
{
    public class GameController : MonoBehaviour
    {
        [HideInInspector] public ClickObject clickObject;
        [SerializeField] private StateMachine stateMachine;
        [SerializeField] private UIController uiController;
        [SerializeField] private SaveSystem saveSystem;
        [SerializeField] private Transform backgroundParent;
        [SerializeField] private Transform clickObjectParent;
        [Header("Bonus")]
        [SerializeField] private Transform bonusParent;
        [SerializeField] private List<BonusController> bonusControllerPrefabs;
        [SerializeField] private float bonusDelay = 5;
        private List<GameObject> activeBonuses = new List<GameObject>();
        
        
        [Space] 
        [SerializeField] private List<GameLevelSO> levels;


        private GameLevelSO level;
        private GameData data;

        private float currTimeInSeconds;
        private float bonusTime;
        private int currProgress;
        private GameObject previousState;

        public float CurrTimeInSeconds
        {
            get => currTimeInSeconds;
            set
            {
                currTimeInSeconds = value;
                if (currTimeInSeconds <= 0)
                {
                    OnLevelFailed();
                    currTimeInSeconds = 0;
                }
                uiController.Timer.SetTimer((int)currTimeInSeconds);
            }
        }

        public int CurrProgress
        {
            get => currProgress;
            set
            {
                currProgress = value;
                uiController.ProgressBar.SetValue(currProgress);
                if (currProgress >= level.ClicksCount)
                    OnLevelComplete();
            }
        }

        public float BonusTime
        {
            get => bonusTime;
            set
            {
                bonusTime = value;
                if (bonusTime < 0)
                {
                    CreateBonus();
                    bonusTime = bonusDelay;   
                }
            }
        }

        public float BonusDelay => bonusDelay;

        public UIController UIController => uiController;

        public Transform ClickObjectParent => clickObjectParent;
        public Transform BackgroundParent => backgroundParent;

        public Transform BonusParent => bonusParent;
        public GameLevelSO Level => level;

        private void Menu()
        {
            previousState = stateMachine.currentState;
            stateMachine.ChangeState("Menu");
        }

        private void Restart()
        {
            stateMachine.ChangeState("Start");
        }

        private void NextLevel()
        {
            data.lastLevel++;
            data.lastLevel %= levels.Count;
            SelectLevel(data.lastLevel);
        }

        private void OnLevelComplete()
        {
            data.UpdateValues(true, level.RequireTime - currTimeInSeconds);
            stateMachine.ChangeState("Win");
            saveSystem.SaveFile(data);
        }

        private void OnLevelFailed()
        {
            data.UpdateValues(false, level.RequireTime - currTimeInSeconds);
            stateMachine.ChangeState("Lose");
            saveSystem.SaveFile(data);
        }

        private void CloseMenu()
        {
            stateMachine.ChangeState(previousState);
        }

        private void SelectLevel(int id)
        {
            data.lastLevel = id;
            level = levels[data.lastLevel];
            stateMachine.ChangeState("Start");
        }
        
        private void CreateBonus()
        {
            var minX = -3.1f;
            var maxX = 1.5f;
            var minY = -4.5f;
            var maxY = 3f;
            float time = 0.2f;
            Vector3 vec = new Vector3(Random.Range(minX, maxX), transform.localPosition.y, Random.Range(minY, maxY));
            var bonusPrefab = bonusControllerPrefabs[Random.Range(0, bonusControllerPrefabs.Count)];
            var go = Instantiate(bonusPrefab, BonusParent);
            go.Init(this);
            go.transform.localPosition = vec;
        }
        
        public void DestroyAllBonuses()
        {
            foreach (Transform child in BonusParent)
            {
                Destroy(child.gameObject);
            }

            activeBonuses = new List<GameObject>();
        }
        
        public void CreateClickObject()
        {
            clickObject = Instantiate(Level.ClickObject, ClickObjectParent);
            clickObject.transform.localPosition = new Vector3(0, 5.34f, 0);   
        }
        
        public void OnBonusClick(Bonus bonus)
        {
            foreach (var activeBonus in activeBonuses)
            {
                if (activeBonus != null && activeBonus.GetComponent(bonus.GetType()))
                {
                    Destroy(activeBonus);
                }
            }
            GameObject bonusGO = new GameObject("bonus");
            bonusGO.transform.SetParent(bonusParent);
            bonusGO.AddComponent(bonus.GetType());
            bonusGO.GetComponent<Bonus>().GetBonus(clickObject);
            activeBonuses.Add(bonusGO);
        }

        private void InitLevelsUIData()
        {
            uiController.LevelsUI.Init(levels, data.levelDatas);
        }
        
        public void Awake()
        {
            uiController.OnMenuButtonClick += Menu;
            uiController.OnRestartButtonClick += Restart;
            uiController.OnNextButtonClick += NextLevel;
            uiController.OnCloseMenuButtonClick += CloseMenu;
            uiController.LevelsUI.OnSelectLevel += SelectLevel;
            
            data = saveSystem.LoadFile() ?? new GameData(levels.Count);
            Debug.Log(data.levelDatas.Length);
            level = levels[data.lastLevel];
            InitLevelsUIData();
            stateMachine.ChangeState("Start");
        }

        public void OnDestroy()
        {
            uiController.OnMenuButtonClick -= Menu;
            uiController.OnRestartButtonClick -= Restart;
            uiController.OnNextButtonClick -= NextLevel;
            uiController.OnCloseMenuButtonClick -= CloseMenu;
            uiController.LevelsUI.OnSelectLevel += SelectLevel;
        }
    }
}
