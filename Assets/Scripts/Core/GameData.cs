using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    [Serializable]
    public class GameData
    {
        public int lastLevel = 0;
        public LevelData[] levelDatas;

        public void UpdateValues(bool isWin, float time)
        {
            levelDatas[lastLevel].attemptsCount++;
            if (isWin)
                levelDatas[lastLevel].successCount++;

            if (levelDatas[lastLevel].bestTime > time || levelDatas[lastLevel].bestTime == 0)
            {
                levelDatas[lastLevel].bestTime = time;
            }
        }

        public GameData(int levelsCount)
        {
            levelDatas = new LevelData[levelsCount];
            for (var index = 0; index < levelDatas.Length; index++)
            {
                levelDatas[index] = new LevelData();
            }
        }
    }

    [Serializable]
    public class LevelData
    {
        public int attemptsCount = 0;
        public int successCount = 0;
        public float bestTime = 0;
        public bool IsCompleted => successCount > 0;
    }
}
