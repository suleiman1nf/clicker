using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Core
{
    public class SaveSystem : MonoBehaviour
    {
        
        public void SaveFile(GameData data)
        {
            string destination = Application.persistentDataPath + "/save.dat";
            FileStream file;
 
            if(File.Exists(destination)) 
                file = File.OpenWrite(destination);
            else 
                file = File.Create(destination);
            
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, data);
            file.Close();
        }
 
        public GameData LoadFile()
        {
            string destination = Application.persistentDataPath + "/save.dat";
            FileStream file;
 
            if(File.Exists(destination)) file = File.OpenRead(destination);
            else
            {
                Debug.Log("File not found, Create empty data");
                return null;
            }
 
            BinaryFormatter bf = new BinaryFormatter();
            GameData data = (GameData) bf.Deserialize(file);
            file.Close();
            
            return data;
        }
    }
}
