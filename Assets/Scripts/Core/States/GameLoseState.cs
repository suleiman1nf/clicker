using System.Collections;
using Pixelplacement;
using UnityEngine;

namespace Core.States
{
    public class GameLoseState : State
    {
        [SerializeField] private GameController gameController;
        public void OnEnable()
        {
            gameController.UIController.ShowLosePanel(true);
        }

        public void OnDisable()
        {
            gameController.UIController.ShowLosePanel(false);
        }
    }
}
