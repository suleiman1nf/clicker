using Core.UI;
using Pixelplacement;
using UnityEngine;

namespace Core.States
{
    public class GameMenuState : State
    {
        [SerializeField] private GameController gameController;

        public void OnEnable()
        {
            gameController.UIController.ShowLevelsPanel(true);
        }

        public void OnDisable()
        {
            gameController.UIController.ShowLevelsPanel(false);
        }
    }
}
