using Pixelplacement;
using UnityEngine;

namespace Core.States
{
    public class GameStartState : State
    {
        [SerializeField] private GameController gameController;
        
        private void OnEnable()
        {
            ChangeBackground();
            DeleteClickObject();
            DeleteBonuses();
            SetTimer();
            SetProgress();
            SetAttemptsCount();
            SetSuccessCount();

            ChangeState("Game");
        }

        private void ChangeBackground()
        {
            foreach (Transform child in gameController.BackgroundParent)
            {
                Destroy(child.gameObject);
            }

            Instantiate(gameController.Level.BackgroundObject, gameController.BackgroundParent);
        }

        private void DeleteClickObject()
        {
            foreach (Transform child in gameController.ClickObjectParent)
            {
                Destroy(child.gameObject);
                gameController.clickObject = null;
            }
        }
        
        private void DeleteBonuses()
        {
            gameController.DestroyAllBonuses();
        }

        private void SetTimer()
        {
            gameController.CurrTimeInSeconds = gameController.Level.RequireTime;
            gameController.BonusTime = gameController.BonusDelay;
        }

        private void SetProgress()
        {
            gameController.UIController.ProgressBar.SetMaxValue(gameController.Level.ClicksCount);
            gameController.CurrProgress = 0;
        }

        private void SetAttemptsCount()
        {
            
        }

        private void SetSuccessCount()
        {
            
        }
    }
}
