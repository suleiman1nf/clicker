using System;
using System.Collections;
using Core.Bonuses;
using Core.ClickModules;
using Pixelplacement;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.States
{
    public class GameState : State
    {
        [SerializeField] private GameController gameController;
        private bool canClick = true;

        private void OnEnable()
        {
            canClick = true;
            if (gameController.clickObject == null)
            {
                gameController.CreateClickObject();
            }
            gameController.clickObject.OnClick += OnClick;
        }

        private void OnDisable()
        {
            if(gameController.clickObject != null)
                gameController.clickObject.OnClick -= OnClick;
        }

        private void OnClick(ClickObject clickObject)
        {
            if (!canClick)
                return;
            clickObject.ExecuteModules(gameController);
        }

        public void Update()
        {
            gameController.CurrTimeInSeconds -= Time.deltaTime;
            gameController.BonusTime -= Time.deltaTime;
        }
    }
}
