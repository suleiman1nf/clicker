using System;
using System.Collections;
using Pixelplacement;
using UnityEngine;

namespace Core.States
{
    public class GameWinState : State
    {
        [SerializeField] private GameController gameController;
        public void OnEnable()
        {
            gameController.UIController.ShowWinPanel(true);
        }

        public void OnDisable()
        {
            gameController.UIController.ShowWinPanel(false);
        }
    }
}
