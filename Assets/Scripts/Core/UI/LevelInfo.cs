using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class LevelInfo : MonoBehaviour
    {
        [SerializeField] private Text tittleText;
        [SerializeField] private Image mainImage;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Text attemptsText;
        [SerializeField] private Text bestTimeText;
        [SerializeField] private Text winsText;

        public void ChangeInfo(string title, Color mainImageColor, Color backgroundImageColor, 
            int attempts, float bestTime, int wins)
        {
            tittleText.text = title;
            this.mainImage.color = mainImageColor;
            this.backgroundImage.color = backgroundImageColor;
            attemptsText.text = "Attempts: " + attempts;
            bestTimeText.text = "Best time: " + (new DateTime().AddSeconds(bestTime)).ToString("mm:ss") ;
            winsText.text = "Wins: " + wins;
        }
    }
}
