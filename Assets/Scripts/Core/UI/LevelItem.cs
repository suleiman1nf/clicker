using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class LevelItem : MonoBehaviour
    {
        [SerializeField] private List<GameObject> stars;
        [SerializeField] private Text levelTitle;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Image clickImage;

        private LevelsUI levelsUI;
        private int id;

        public void Init(LevelsUI levelsUI, int id)
        {
            this.levelsUI = levelsUI;
            this.id = id;
        }

        public void SetDifficulty(int count)
        {
            if(count >= stars.Count)
                Debug.LogError("big value");

            foreach (var star in stars)
            {
                star.SetActive(false);
            }

            for (int i = 0; i < count; i++)
            {
                stars[i].SetActive(true);
            }
        }

        public void SetTitle(int num)
        {
            levelTitle.text = num.ToString();
        }

        public void OnClick()
        {
            levelsUI.SetLevel(id);
        }

        public void SetBackgroundColor(Color color)
        {
            backgroundImage.color = color;
        }

        public void SetClickImage(Sprite sprite)
        {
            clickImage.sprite = sprite;
        }
    }
}
