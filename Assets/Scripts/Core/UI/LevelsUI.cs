using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.UI
{
    public class LevelsUI : MonoBehaviour
    {
        [SerializeField] private Transform parent;
        [SerializeField] private LevelItem prefab;
        [SerializeField] private PopupWindow levelWindow;
        [SerializeField] private LevelInfo levelInfo;
        public event Action<int> OnSelectLevel;

        private int selectedLevel = 0;
        private LevelData[] levelDatas; 
        
        public void Init(List<GameLevelSO> objectsData, LevelData[] levels)
        {
            foreach (Transform child in parent)
            {
                Destroy(child.gameObject);
            }

            for (int i = 0; i < objectsData.Count; i++)
            {
                LevelItem go = Instantiate(prefab, parent);
                go.Init(this, i);
                go.SetDifficulty(objectsData[i].Difficulty);
                go.SetTitle(i+1);
            }

            levelDatas = levels;
        }

        public void SetLevel(int levelId)
        {
            selectedLevel = levelId;
            levelInfo.ChangeInfo(
                "Level " + (levelId+1),
                Color.blue, 
                Color.cyan,
                levelDatas[levelId].attemptsCount, 
                levelDatas[levelId].bestTime,
                levelDatas[levelId].successCount);
            levelWindow.Open();
        }

        public void OpenLevel()
        {
            levelWindow.Close();
            OnSelectLevel?.Invoke(selectedLevel);
        }
    }
}
