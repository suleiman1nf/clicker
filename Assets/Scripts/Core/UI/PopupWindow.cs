using System;
using DG.Tweening;
using UnityEngine;

namespace Core.UI
{
    public class PopupWindow : MonoBehaviour
    {
        private float time = 0.2f;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private GameObject blocker;
        
        private void OnEnable()
        {
            canvasGroup.alpha = 1;
            transform.localScale = Vector3.zero;
            transform.DOScale(Vector3.one, time).SetEase(Ease.OutBack);
        }

        public void Open()
        {
            blocker.SetActive(true);
        }

        public void Close()
        {
            canvasGroup.DOFade(0, time);
            transform.DOScale(Vector3.zero, time).SetEase(Ease.InBack).OnComplete(()=>blocker.gameObject.SetActive(false));
        }
    }
}
