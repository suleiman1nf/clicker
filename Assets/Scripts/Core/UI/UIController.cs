using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private ProgressBar progressBar;
        [SerializeField] private Timer timer;
        
        [SerializeField] private PopupWindow winPanel;
        [SerializeField] private PopupWindow losePanel;
        [SerializeField] private PopupWindow levelsPanel;
        [SerializeField] private LevelsUI levelsUI;
        
        public event Action OnMenuButtonClick;
        public event Action OnRestartButtonClick;
        public event Action OnNextButtonClick;
        public event Action OnCloseMenuButtonClick;
        public ProgressBar ProgressBar => progressBar;

        public LevelsUI LevelsUI => levelsUI;
        public Timer Timer => timer;
        
        public void MenuButtonClick()
        {
            OnMenuButtonClick?.Invoke();
            levelsUI.gameObject.SetActive(true);
        }

        public void RestartClick()
        {
            OnRestartButtonClick?.Invoke();
        }

        public void NextButtonClick()
        {
            OnNextButtonClick?.Invoke();
        }

        private void ShowPanel(PopupWindow window, bool state)
        {
            if(state)
                window.Open();
            else
            {
                window.Close();
            }
        }

        public void CloseMenuClick()
        {
            OnCloseMenuButtonClick?.Invoke();
        }

        public void ShowWinPanel(bool state)
        {
            ShowPanel(winPanel, state);
        }
        
        public void ShowLosePanel(bool state)
        {
            ShowPanel(losePanel, state);
        }
        public void ShowLevelsPanel(bool state)
        {
            ShowPanel(levelsPanel, state);
        }
    }

    [Serializable]
    public class ProgressBar
    {
        [SerializeField] private Slider slider;
        [SerializeField] private Text text;
        
        public void SetMaxValue(int val)
        {
            slider.maxValue = val;
            SetTextValue(0, val);
        }

        public void SetValue(int val)
        {
            slider.value = val;
            SetTextValue(val, (int)slider.maxValue);
        }

        private void SetTextValue(int currValue, int maxValue)
        {
            text.text = $"{currValue} / {maxValue}";
        }
    }

    [Serializable]
    public class Timer
    {
        [SerializeField] private Text text;
        
        public void SetTimer(DateTime time)
        {
            text.text = time.ToString("mm:ss");
        }

        public void SetTimer(int timeInSeconds)
        {
            SetTimer(new DateTime().AddSeconds(timeInSeconds));
        } 
    }
}
