using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Core.UI
{
    public class VictoryStars : MonoBehaviour
    {
        [SerializeField] private List<Transform> stars;
        [SerializeField] private Ease ease;
        
        public void OnEnable()
        {
            foreach (var star in stars)
            {
                star.localScale = Vector3.zero;
            }
            ShowStars();
        }

        private void ShowStars()
        {
            Sequence sequence = DOTween.Sequence();

            foreach (var star in stars)
            {
                sequence.Append(star.DOScale(Vector3.one, 0.8f).SetEase(ease));
            }
        }
    }
}
