using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameLevel", order = 1)]
public class GameLevelSO : ScriptableObject
{
    [SerializeField] private int difficulty;
    [SerializeField] private int clicksCount;
    [SerializeField] private int requireTime;
    [SerializeField] private ClickObject clickObject;
    [SerializeField] private GameObject backgroundObject;

    public int Difficulty => difficulty;

    public int ClicksCount => clicksCount;

    public int RequireTime => requireTime;

    public ClickObject ClickObject => clickObject;

    public GameObject BackgroundObject => backgroundObject;
}
